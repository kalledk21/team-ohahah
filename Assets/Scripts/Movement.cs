﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    Transform Player;
    [SerializeField]
    float Speed;

    [SerializeField]
    float TurnSpeed;

    [SerializeField]
    Rigidbody rb;

    void Awake()
    {
        Player = this.transform;
        rb = this.GetComponent<Rigidbody>();
    }

	void Start ()
    {
        
	}
	
	
	void Update ()
    {
        rb.velocity = new Vector3(0, 0, 0);

        Player.Translate(Vector3.forward * Speed * Input.GetAxisRaw("Vertical") * Time.deltaTime);
        Player.Rotate(Vector3.up, Input.GetAxisRaw("Horizontal") * TurnSpeed * Time.deltaTime);
	}
}
