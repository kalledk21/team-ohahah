﻿using UnityEngine;
using System.Collections;

public class PlayerG_Animation : MonoBehaviour {

	private float vert;
	private bool hori;
	private Animator anim;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		vert = Input.GetAxisRaw("Vertical");
		anim.SetFloat ("walk", vert);

		if (Input.GetKeyDown(KeyCode.Space)) 
		{
			anim.SetBool("IsGrouned", false);
		}else {
			anim.SetBool("IsGrouned", true);
		}
		if (Input.GetMouseButtonDown(0)) 
		{
			anim.SetBool("Attack", true);
		}else {
			anim.SetBool("Attack", false);
		}
		if (Input.GetMouseButtonDown(1)) 
		{
			anim.SetBool("Attack2", true);
		}else {
			anim.SetBool("Attack2", false);
		}
		if (Input.GetMouseButtonDown(2)) 
		{
			anim.SetBool("Attack3", true);
		}else {
			anim.SetBool("Attack3", false);
		}
	}
}
