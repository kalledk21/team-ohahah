﻿using UnityEngine;
using System.Collections;

public class Music_Playlist : MonoBehaviour {

	public bool ActivateOnAwake = true;
	public AudioClip[] MusicList;
	
	void Awake() {
		if (ActivateOnAwake && Music_Manager.Instance)
			Music_Manager.Instance.ChangePlaylist (this);
	}
	
	void Start () {
		// Have playlist persist across scenes.
		DontDestroyOnLoad (gameObject); // Don't destroy this object
		
		// When a new scene is loaded, destroy the other playlists.
		foreach (Music_Playlist playlist in GameObject.FindObjectsOfType<Music_Playlist>()) {
			if (playlist.name != this.name) {
				Destroy (playlist.gameObject);
			}
		}
	}
	
}
