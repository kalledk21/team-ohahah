﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class EnemyMove : MonoBehaviour {

    [SerializeField]
    Transform Player;

    [SerializeField]
    public float engageDist;

    [SerializeField]
    public float hitCooldown;

    [SerializeField]
    public float collRad;

    public float speed;
    public float dist;
    public float oldDist;
    public float nextHitTime;

    public float damage;
    public float health;

    public Player pl;

    Vector3 pos;
    Vector3 oldPos;

    public float deltaForward;

    public Enermy_Animation anim_enemy;
    
    void Awake()
    {
        Player = GameObject.Find("Player").transform;
        pl = Player.GetComponent<Player>();
        anim_enemy = GetComponent<Enermy_Animation>();
    }

	void FixedUpdate ()
    {
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }

        pos = transform.position;

        dist = Vector3.Distance(transform.position, Player.position);
        speed = 0.5f + (Vector3.Distance(transform.position, Player.position)/200f);
        transform.LookAt(Player);
        if (dist > engageDist)
        {
            this.GetComponent<Rigidbody>().MovePosition(transform.position + (transform.forward * speed));
            //transform.Translate(Vector3.forward*speed);
        }
        if (dist <= engageDist && Time.time >= nextHitTime)
        {
            anim_enemy.hi();
            nextHitTime = Time.time + hitCooldown;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            pl.Health -= damage;
        }
        

        deltaForward = Vector3.Distance(pos, oldPos);

        oldDist = Vector3.Distance(transform.position, Player.position);
        oldPos = transform.position;
	}

    void OnDrawGizmos()
    {
        Handles.DrawWireDisc(transform.position, Vector3.up, collRad);
        Handles.DrawLine(transform.position, transform.position + (transform.forward * dist));
    }

    void OnTriggerStay()
    {
        health -= Random.Range(2f, 3f);
    }
}
