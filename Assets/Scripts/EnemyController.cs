﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyController : MonoBehaviour {

    [SerializeField]
    float RoundNr;
    float RoundPoints;

    [SerializeField]
    GameObject Prefab;

    [SerializeField]
    GameObject holder;

    [SerializeField]
    float distance;

    public Text txt;

    void Start()
    {
        NextRound();
    }

    void Update()
    {
        if(holder.transform.childCount == 0)
        {
            NextRound();
        }
    }

    void NextRound()
    {
        while (RoundPoints < RoundNr)
        {
            SpawnEnemy(Random.Range(10f, 15f), Random.Range(5f, 10f), Random.Range(8f, 11f));
            
        }
        RoundNr++;
        RoundPoints = 0;
    }
    void SpawnEnemy(float h, float d, float s)
    {
        float angle = Random.Range(-Mathf.PI, Mathf.PI);

        Vector3 circle = new Vector3(Mathf.Cos(angle) * distance, 0, Mathf.Sin(angle) * distance);

        GameObject enemyGO = Instantiate(Prefab, circle, Quaternion.identity) as GameObject;

        Enemy enemy = new Enemy(h, d, s);
        RoundPoints += (float)enemy.tier / 10f;

        enemyGO.GetComponent<EnemyMove>().damage = enemy.Damage;
        enemyGO.GetComponent<EnemyMove>().health = enemy.Health;

        enemyGO.transform.LookAt(new Vector3(0, 0, 0));
        enemyGO.transform.localScale = new Vector3(0.5f + ((float)enemy.tier / 30f), 0.5f + ((float)enemy.tier / 30f), 0.5f + ((float)enemy.tier / 30f));
        enemyGO.transform.parent = holder.transform;
    }

    void LateUpdate()
    {
        txt.text = holder.transform.childCount.ToString();
    }
}
