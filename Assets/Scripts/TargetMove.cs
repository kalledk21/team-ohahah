﻿using UnityEngine;
using System.Collections;

public class TargetMove : MonoBehaviour {

    [SerializeField]
    Vector3 move;

    [SerializeField]
    float speed;

    [SerializeField]
    float turnspeed;

    

    

	void Update ()
    {
        

        move = new Vector3(0, 0, Input.GetAxisRaw("Vertical"));

        transform.Rotate(Vector3.up, Input.GetAxisRaw("Horizontal") * Time.deltaTime * turnspeed);
        transform.Translate(move * speed * Time.deltaTime);
	}
}
