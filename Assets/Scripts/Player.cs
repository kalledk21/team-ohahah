﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {
    [SerializeField]
    float health = 400f;
    bool alive = true;
    float startHealth;

    public Text txt;

    public Image bar;

    public float Health
    {
        get 
        {
            return health; 
        }
        set
        {
            health = value;
            if (health <= 0)
            {
                health = 0;
            }
        }        
    }

    void Start()
    {
        startHealth = health;
    }

    void LateUpdate()
    {
        bar.fillAmount = health / startHealth;

        if (health <= 1)
        {
            alive = false;
        }
        txt.text = ((int)health).ToString();
    }

    void FixedUpdate()
    {
        if (alive && health < startHealth)
        {
            health += 0.025f;
        }
    }
}
