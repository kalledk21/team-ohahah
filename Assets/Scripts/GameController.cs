﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour {

    public Player pl;

    void LateUpdate()
    {
        if (pl.Health <= 0)
        {
            SceneManager.LoadScene(3);
        }
    }
}
