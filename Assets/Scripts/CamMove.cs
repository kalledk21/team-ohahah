﻿using UnityEngine;
using System.Collections;

public class CamMove : MonoBehaviour {
    
    [SerializeField]
    Transform target;

    [SerializeField]
    float distance;

    [SerializeField]
    float turnX;

    [SerializeField]
    float turnY;

    [SerializeField]
    Vector3 offset;

    private float xmove;
    private float ymove;

    [SerializeField]
    float scrollSpeed;

    [SerializeField]
    int minScroll;
    [SerializeField]
    int maxScroll;
    
    void Start()
    {
        distance = (target.position + transform.position).magnitude;
    }

    void LateUpdate()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
        {
            distance = Mathf.Clamp(distance- Input.GetAxisRaw("Mouse ScrollWheel") * scrollSpeed, minScroll, maxScroll);
        }

        transform.position = target.transform.position - distance * target.transform.forward + offset;
        transform.LookAt(target.transform);
        
        xmove += Input.GetAxisRaw("Mouse X");
        ymove -= Input.GetAxisRaw("Mouse Y");
        transform.RotateAround(target.transform.position, Vector3.up, xmove*turnX);
        transform.RotateAround(target.transform.position, transform.right, ymove*turnY);
    }
}
