﻿using UnityEngine;
using System.Collections;

public class Main_Menu : MonoBehaviour {

	public void Mein_Menu ()
	{
		Application.LoadLevel (0);
	}

	public void NewGame ()
	{
		Application.LoadLevel (1);
	}
	public void ResumeGame ()
	{
		Application.LoadLevel (1);
	}
	public void Options ()
	{
		Application.LoadLevel (2);
	}
	public void Store ()
	{
		Application.LoadLevel (3);
	}
	public void QuitGame ()
	{
		Application.Quit();
	}
}
