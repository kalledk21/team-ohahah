﻿using UnityEngine;
using System.Collections;

public class Enemy {

    public float Health;
    public float Damage;
    public float Speed;

    public enum Tier { t1 = 10, t2 = 15, t3 = 25, t4 = 40, t5 = 60};

    public Tier tier;

    public Enemy(float health, float damage, float speed)
    {
        Health = health;
        Damage = damage;
        Speed = speed;

        if (health * damage * speed <= 650f)
        {
            tier = Tier.t1;
        }
        else if (health * damage * speed <= 900f)
        {
            tier = Tier.t2;
        }
        else if (health * damage * speed <= 1150f)
        {
            tier = Tier.t3;
        }
        else if (health * damage * speed <= 1400f)
        {
            tier = Tier.t4;
        }
        else
        {
            tier = Tier.t5;
        }
    }
}
