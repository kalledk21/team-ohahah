﻿using UnityEngine;
using System.Collections;

public class Enermy_Animation : MonoBehaviour
{

    EnemyMove moveScript;
    Animator anim;

    void Start()
    {
        moveScript = GetComponent<EnemyMove>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Speed", moveScript.deltaForward);
    }

    public void hi()
    {
        int rng = Random.Range(0, 3);
        Debug.Log("Triggered");
        if (rng == 1)
        {
            anim.SetBool("Attack", true);
        }
        else
        {
            anim.SetBool("Attack", false);
        }
        if (rng == 2)
        {
            anim.SetBool("Attack2", true);
        }
        else {
            anim.SetBool("Attack2", false);
        }
        if (rng == 3)
        {
            anim.SetBool("Attack3", true);
        }
        else
        {
            anim.SetBool("Attack3", false);
        }
    }
}
